section .text

; int itoa_int(long number, char* ptr, byte radix)
; Converts 64-bit number to string
;
; @param[rdi] number    qword number to convert
; @param[rsi] ptr       pointer to buffer to write to
; @param[rdx] radix     radix to use
; @return[rax]          used buffer length (including NUL terminator) or 0 if error
global itoa_int
itoa_int:
    test rdi, rdi
    jns .continue

    mov byte [rsi], '-'
    inc rsi
    neg rdi

.continue:
    jmp itoa_uint


; int itoa_uint(long number, char* ptr, byte radix)
; Converts 64-bit number to string
;
; @param[rdi] number    unsigned qword number to convert
; @param[rsi] ptr       pointer to buffer to write to
; @param[rdx] radix     radix to use
; @return[rax]          used buffer length (including NUL terminator) or 0 if error
global itoa_uint
itoa_uint:
    mov rax, rdi        ; rax - number
    mov rdi, rdx        ; rdi - radix
    xor rcx, rcx        ; rcx - result symbol count

    cmp rdx, 2
    jb .err
    cmp rdx, 36         ; unsupported radix
    ja .err

.loop:
    xor rdx, rdx
    div rdi             ; rdx = rax % rdi; rax = rax / rdi

    add rdx, '0'        ; rdx < 10 ? (rdx + '0') : (rdx - 10 + 'a')
    cmp rdx, '9'
    jbe .next_char
    add rdx, 'a' - '0' - 10

.next_char:
    mov [rsi+rcx], rdx
    inc rcx

    test rax, rax       ; check number end
    jnz .loop

.end_ok:
    mov byte [rsi+rcx], 0

    push rcx            ; to be made rax as result
    mov rdi, rsi
    call string_reverse
    pop rax

    jmp .end
.err:
    xor rax, rax
.end:
    ret


