section .text

; long string_length(char* ptr)
; Calculate length of c-string
;
; @param[rdi]   ptr     pointer to string
; @return[rax]          length
global string_length
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; void string_reverse(char* str)
; Reverse src string inplace
;
; @param[rdi] str       input string & output buffer
global string_reverse
string_reverse:
    push rdi
    call string_length
    pop rdi

    mov rsi, rdi        ; for back pointer
    add rsi, rax
    dec rsi
    shr rax, 1          ; swaps count

    xor rcx, rcx
.loop:
    ; exchange by pairs
    mov  dl, byte [rdi+rcx]
    xchg dl, byte [rsi]
    mov  byte [rdi+rcx], dl
    dec rsi
    inc rcx
    cmp rcx, rax
    jb .loop

    xor rax, rax
    ret


; long string_copy(char* src, char* dst, long dst_size)
; Copy string from src to dst buffer of specified length
;
; @param[rdi]   src         source c-string string pointer
; @param[rsi]   dst         target buffer pointer
; @param[rdx]   dst_size    buffer size in bytes
; @return[rax]  string length or 0 if it exceeds buffer size
global string_copy
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rdi ; swap rsi and rdi to match their meaning
    pop rsi

    cmp rax, rdx
    jae .err                    ; rax string len without NUL

    mov rcx, rax                ; set copy length
    inc rcx

    rep movsb

;.loop:
;    mov dl, byte [rsi+rcx]
;    mov byte [rdi+rcx], dl
;    dec rcx
;    test rcx, rcx
;    jns .loop

    jmp .end

.err:
    xor rax, rax
.end:
    ret


; bool string_equals(char* str1, char* str2)
; Compare 2 strings
;
; @param[rdi]   src         c-string string pointer
; @param[rsi]   dst         c-string string pointer
; @return[rax]              1 if equals 0 otherwise
global string_equals
string_equals:
    xor rcx, rcx
.loop:
    mov byte dl, [rsi+rcx]
    cmp byte dl, [rdi+rcx]
    jne .not_equals

    test dl, dl
    jz .equals

    inc rcx
    jmp .loop

.not_equals:
    xor rax, rax
    jmp .end
.equals:
    mov rax, 1
.end:
    ret


