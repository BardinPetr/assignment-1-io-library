section .text

; void print_char(byte symbol)
; Prints one byte symbol to stdout
; @param[rdi]     symbol
global print_char
print_char:
    enter 1, 0

    mov [rbp-1], dil

    mov rax, SYS_WRITE
    mov rdi, FD_STDOUT
    lea rsi, [rbp-1]
    mov rdx, 1
    syscall

    leave
    ret


; void print_newline()
; Prints \n to stdout
global print_newline
print_newline:
    mov rdi, 0xA
    jmp print_char


; void print_string_fd(char* ptr, int fd)
; Print string to specified fd
; @param[rdi] ptr    pointer to string
; @param[rsi] fd     file descriptor
print_string_fd:
    push rdi
    push rsi
    call string_length  ; len in rax

    pop rdi             ; rsi -> rdi: fd
    pop rsi             ; rdi -> rsi: buf
    mov rdx, rax        ; rax -> rdx: count

    mov rax, SYS_WRITE
    syscall
    ret


; void print_string(char* ptr)
; Print c-string to stdout
; @param[rdi] ptr    pointer to string
global print_string
print_string:
    mov rsi, FD_STDOUT
    jmp print_string_fd


; void print_error(char* ptr)
; Print string to stderr
; @param[rdi] ptr    pointer to string
global print_error
print_error:
    mov rsi, FD_STDERR
    jmp print_string_fd


; void print_line(char* ptr)
; Prints string and \n at the end
; @param[rdi] ptr    pointer to string
global print_line
print_line:
    call print_string
    jmp print_newline

; void print_int(long number)
; Prints number to stdout
;
; @param[rdi] number    number to convert
; @return[rax]          used buffer length (including NUL terminator) or 0 if error
global print_int
print_int:
    test rdi, rdi
    jns .continue

    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

.continue:
    jmp print_uint


; void print_uint(long number)
; Prints unsigned number to stdout
;
; @param[rdi] number    number to convert
; @return[rax]          used buffer length (including NUL terminator) or 0 if error
%define PRINT_UINT_BUFFER 22
global print_uint
print_uint:
    ; 64-bit decimal number may take up 22 chars (including sign & NUL)
    enter PRINT_UINT_BUFFER, 0

    mov rsi, rsp

    push rsi            ; string pointer
    mov rdx, 10
    call itoa_uint
    pop rdi

    push rax
    call print_string
    pop rax

    leave
    ret
