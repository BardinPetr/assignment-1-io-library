section .text

; (long, long) parse_int(char* ptr)
; Parse 64-bit signed integer from string
;
; @param[rdi]   ptr     pointer to string
; @returns[rax]         parsed integer
; @returns[rdx]         number length in decimal symbols or 0 if failed to parse
global parse_int
parse_int:
    movsx rcx, byte [rdi]           ; check if signed
    cmp rcx, '-'
    je  .signed
    cmp rcx, '+'
    mov rcx, 0                      ; for reuse
    je  .unsigned_plus
    jmp .unsigned

    ; RCX:  2 for negative, 1 for positive with '+' sign, 0 for positive no sign

.signed:
    mov rcx, 1

.unsigned_plus:
    inc rdi                         ; skip +\-
    inc rcx

.unsigned:
    push rcx
    call parse_uint
    pop rcx

    test rdx, rdx
    jz .err

    test rax, rax
    js .err                         ; no space left for sign bit

    cmp rcx, 2
    jne .continue_unsigned
    neg rax

.continue_unsigned:
    test rcx, rcx
    jz .end
    inc rdx                         ; add to number symbols count sign character
    jmp .end

.err:
    xor rax, rax
    xor rdx, rdx

.end:
    ret


; (long, long) parse_uint(char* ptr)
; Parse unsigned 64-bit integer from string
; Number input could be terminated without error by non-digit symbol after at least one digit
;
; @param[rdi]   ptr     pointer to string
; @returns[rax]         parsed integer
; @returns[rdx]         number length in decimal symbols or 0 if failed to parse (invalid symbol or unsigned qword overflow)
global parse_uint
parse_uint:
    xor rax, rax            ; result
    xor rdx, rdx            ; counter
    xor rcx, rcx            ; digit

    .loop:
        movsx rcx, byte [rdi+rdx]   ; symbol

        test rcx, rcx
        jz .end                     ; string end

        cmp rcx, '0'                ; check ranges
        jb .err_range
        cmp rcx, '9'
        ja .err_range

        inc rdx

        sub rcx, '0'                ; decode ASCII
        imul rax, 10
        jc .err

        add rax, rcx

        jmp .loop

.err_range:
    ; should allow number-starting string to end with any symbols
    test rdx, rdx
    jnz .end         ; more than one valid digit => end successfully

.err:
    xor rdx, rdx
    xor rax, rax

.end:
    ret

