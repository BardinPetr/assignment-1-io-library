%define CHAR_LF 0x0A
%define CHAR_HT 0x09
%define CHAR_SP 0x20

section .text

; char read_char()
; Reads single byte from STDIN and returns its value or 0 if face EOF
; @returns[rax]     symbol or 0
global read_char
read_char:
    enter 1, 0

    mov rax, SYS_READ
    mov rdi, FD_STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall

    cmp rax, 0          ; check eof or errors
    movsx rax, byte [rbp-1]

    jg .normal_end
    xor rax, rax

.normal_end:
    leave
    ret


; char read_char_checked()
; Reads char from STDIN and performs space-symbol check
;
; @returns[rax]     read char, or -1 if EOF, 0 if in {0x09, 0x0A, 0x20}
read_char_checked:
    call read_char
    test rax, rax
    jz .eof

    cmp rax, CHAR_LF
    je .space
    cmp rax, CHAR_HT
    je .space
    cmp rax, CHAR_SP
    je .space
    jmp .end

.space:
    xor rax, rax
    jmp .end
.eof:
    mov rax, -1
.end:
    ret



; (int, int) read_word(char* ptr, int len)
; Reads space (09h, 0Ah, 20h) delimited word from STDIN skipping leading spaces as c-string
;
; @param[rdi] ptr   buffer pointer
; @param[rsi] len   buffer length
; @returns[rax]     buffer pointer or 0 if failed
; @returns[rdx]     word length including NUL-terminator
global read_word
read_word:
    dec rsi            ; reserve one symbol for NUL
    cmp rsi, 0
    jle .exit_err      ; buffer len <= 1

    xor rdx, rdx       ; symbol counter

    .loop:
        push rdi
        push rsi
        push rdx
        call read_char_checked
        pop rdx
        pop rsi
        pop rdi

        test rax, rax
        js .exit_normal

        jz .char_space
        ; normal symbol
            cmp rdx, rsi
            jae .exit_err             ; buffer overflow

            mov byte [rdi+rdx], al
            inc rdx
            jmp .loop

        .char_space:
        ; space symbol
            test rdx, rdx
            jz .loop                 ; no data symbols are read yet => skipping spaces
            jmp .exit_normal         ; space after data => stop


.exit_err:
    xor rax, rax
    xor rdx, rdx
    jmp .exit
.exit_normal:
    mov byte [rdi+rdx], 0            ; end string
    mov rax, rdi
.exit:
    ret


; int read_line(char* ptr, int len)
; Reads string from STDIN until \n. \n is not included in output
;
; @param[rdi] ptr   buffer pointer
; @param[rsi] len   buffer length
; @returns[rax]     string length including \0 or 0 if error or buffer overflow
global read_line
read_line:
    enter 8, 0
    push rbx
    push r12
    mov [rbp-8], rsi
    mov rbx, rdi
    mov r12, 0

    cmp rsi, 2
    jb .exit_err    ; check buffer size > 1

.loop:
    call read_char

    test al, al
    jz .exit_ok
    cmp al, CHAR_LF
    je .exit_ok

    mov byte [rbx+r12], al
    inc r12

    cmp r12, [rbp-8]
    jae .exit_err

    jmp .loop

.exit_err:
    xor rax, rax
    jmp .exit

.exit_ok:
    mov byte [rbx+r12], 0
    inc r12
    mov rax, r12

.exit:
    pop r12
    pop rbx
    leave
    ret
