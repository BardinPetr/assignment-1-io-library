OUT = build
ASM = nasm -felf64 -g
LD = ld

.phony: clean all $(OUT)
all: $(OUT)/lib.o $(OUT)/lib.inc

$(OUT):
	mkdir -p $@

$(OUT)/lib.o: $(wildcard ./src/*) lib.asm | $(OUT)
	$(ASM) -o $(OUT)/lib.o lib.asm

$(OUT)/lib.inc: lib.inc | $(OUT)
	cp lib.inc $(OUT)/

$(OUT)/main.o: main.asm | $(OUT)
	$(ASM) -o $(OUT)/main.o main.asm

$(OUT)/main: $(OUT)/main.o $(OUT)/lib.o
	$(LD) -o $(OUT)/main $(OUT)/lib.o $(OUT)/main.o

run: $(OUT)/main
	./$(OUT)/main

clean:
	rm -rf $(OUT)/*
