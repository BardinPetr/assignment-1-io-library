%define SYS_READ    0
%define SYS_WRITE   1
%define SYS_EXIT    60

%define FD_STDIN    0
%define FD_STDOUT   1
%define FD_STDERR   2

%include './src/string.inc'
%include './src/atoi.inc'
%include './src/itoa.inc'
%include './src/input.inc'
%include './src/output.inc'


section .text

; void exit(int code)
; Exits current process with supplied return code
;
; @param[rdi]     code
global exit
exit:
    mov rax, SYS_EXIT
    syscall
    ret

